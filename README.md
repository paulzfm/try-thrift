### Dependences

- thrift: https://thrift.apache.org/download
- nodejs: https://nodejs.org/en/download/

### Generate Codes

	thrift --gen js:node tutorial.thrift

### Install Packages

	npm install

### Start Server

	node server.js

### Start Client

	node client.js
