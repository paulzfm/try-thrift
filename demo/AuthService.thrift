namespace java me.ipwx.sola.core.service
namespace py me.ipwx.sola.core.service

include "AuthTypes.thrift"
include "UserTypes.thrift"

/**
 * The user credential for a client to request a token.
 */
struct ApiUserCredential {
  /**
   * In Sola ML platform, a user is identified by a domain and a username.
   * However, a default domain should have been configured in each running system, so the
   * client may neglect this field so as to use that default domain.
   */
  1: optional string domain;

  /** Just fill in the username provided by the user. */
  2: required string name;

  /** User password in plain text. */
  3: required string password;
}

/** Group information contained in an [[ApiAuthTokenInfo]]. */
struct ApiAuthGroup {
  /** UUID of this group. */
  1: required string id;

  /** Whether or not this group is an administrator group. */
  2: optional bool isAdmin = false;
}

/** User information contained in an [[ApiAuthTokenInfo]]. */
struct ApiAuthUser {
  /** UUID of this user. */
  1: required string id;

  /** Whether or not this user has been disabled. */
  2: optional bool disabled = false;

  /** The groups of this authenticated user. */
  3: optional list<ApiAuthGroup> groups;
}

/**
 * Authentication token object.
 *
 * In Sola ML, an auth token must be encrypted, signed and encoded in base64 before it
 * is transferred to the clients.  The client will never know the secret key, so it will
 * not be able to forge a token by itself.  However, the clients may need the information
 * from the token, e.g., to know the stale time and the expire time.  So this structure
 * carries all the information about a token needed by a client.
 */
struct ApiAuthTokenInfo {
  /** The user information of this token. */
  1: required ApiAuthUser user;

  /**
   * The token will become stale after this time.
   *
   * Time value is given by a long, which representing the milliseconds from the zero GMT
   * (or to say, the milliseconds from January 1, 1970, 00:00:00 GMT).
   */
  2: required i64 staleTime;

  /**
   * The token will become expired after this time.
   *
   * Time value is given by a long, which representing the milliseconds from the zero GMT
   * (or to say, the milliseconds from January 1, 1970, 00:00:00 GMT).
   */
  3: required i64 expireTime;
}

/**
 * An auth token string with information.
 *
 * In Sola ML, an auth token must be encrypted, signed and encoded in base64 before it
 * is transferred to the clients.  The client will never know the secret key, so the server
 * can validate the tokens provided by clients.
 */
struct ApiAuthTokenWithInfo {
  /** The encoded (encrypted) token string. */
  1: required string token;

  /** The token information. */
  2: required ApiAuthTokenInfo info;
}

/**
 * Thrift service definition for user authentication.
 *
 * Almost all APIs in Sola ML requires the client to send an AuthToken.
 * It will accept or decline the client request according to such token.
 * So the client must apply a token from server before any other action.
 *
 * The token will not live forever.  It will become stale periodically,
 * and eventually it will become expired.  A stale token can be refreshed
 * through a very simple API, without the need for relogin; however, when
 * a token has become expired, it cannot be refreshed again, and the client
 * have to use the username and password to get a new token.
 *
 * Not all stale tokens can be refreshed successfully.  If a token failed
 * to be refreshed, the clients must request a new token with username and
 * password.
 *
 * The time for a token to become stale is usually around 15 minutes.  It
 * takes a much longer time, usually 7 days, for it to become expired.
 */
service AuthService {
  /** Request a new token from Sola ML system with the user credential. */
  ApiAuthTokenWithInfo requestToken(1: ApiUserCredential credential) throws (1: AuthTypes.ApiAuthError authError);

  /**
   * Refresh a (maybe) stale token and get the new token.
   *
   * This method can be called even with a fresh token, and it will produce a new token more
   * fresh than the provided one.
   */
  ApiAuthTokenWithInfo refreshToken(1: string oldToken) throws (1: AuthTypes.ApiAuthError authError);

  /** Abandon a token, so that it will become invalid immediately. */
  void abandonToken(1: string token) throws (1: AuthTypes.ApiAuthError authError);

  /** Get the information of user associated with specified token. */
  UserTypes.ApiUserInfo getTokenUser(1: string token) throws (1: AuthTypes.ApiAuthError authError);
}