namespace java me.ipwx.sola.core.service
namespace py me.ipwx.sola.core.service

/**
 * The enumeration of the authentication error type.
 *
 * Every type of authentication error would be thrown by any API method if the
 * corresponding situation takes place.
 */
enum ApiAuthErrorType {
  // The token provided by the client is malformed or has become expired.
  INVALID_TOKEN = 1,

  // The client is requesting a token with invalid username / password.
  INVALID_CREDENTIAL = 2,

  // The credential is valid, but the user has been disabled.
  CREDENTIAL_DISABLED = 3,

  // The token provided by the client is stale, so it must be refreshed.
  STALE_TOKEN = 4,

  // The client attempts to refresh a stale token, but the system found that token
  // has been expired or invalidated by some other cause, so it cannot be refreshed
  // again.
  CANNOT_REFRESH_TOKEN = 5,

  // The client provided a fresh token, but the owner of the token does not exist.
  // Such error may be caused when the corresponding user is deleted.
  TOKEN_USER_INEXIST = 6,

  // The client provides a valid token, but it does not have the privileges to
  // perform the requested operation.
  TOKEN_INSUFFICIENT_PRIVILEGE = 7
}

/**
 * Error to indicate one of the situations defined by [[ApiAuthErrorType]].
 */
exception ApiAuthError {
  /** The error type of this authentication error. */
  1: ApiAuthErrorType errorType;
}