namespace java me.ipwx.sola.core.service
namespace py me.ipwx.sola.core.service

/**
 * Information about a group.  Domains and usages are similar to [[ApiUserInfo]].
 */
struct ApiGroupInfo {
  /** UUID of this group. */
  1: required string id;

  /** Domain of this group. */
  2: required string domain;

  /** Name of this group. */
  3: required string name;

  /** Whether or not this group is an administrator group. */
  4: optional bool isAdmin = false;
}

/**
 * Information about a user.
 *
 * Objects of this type are usually created by the server to carry the information of
 * a particular user.  It may be revised time after time, so optional fields are preferred.
 */
struct ApiUserInfo {
  /** UUID of this user. */
  1: required string id;

  /** Domain of this user. */
  2: required string domain;

  /** Name of this user, which is used in the credential when requesting a token. */
  3: required string name;

  /** The fullname of this user. */
  4: optional string fullName;

  /** The email of this user. */
  5: optional string email;

  /** Whether or not this user has been disabled. */
  6: optional bool disabled = false;

  /** The groups of this user. */
  7: optional list<ApiGroupInfo> groups;
}
