var thrift = require('thrift');
var AuthService = require('./gen-nodejs/AuthService');
var types = require('./gen-nodejs/AuthService_types');

var transport = thrift.TBufferedTransport();
var protocol = thrift.TBinaryProtocol();

var connection = thrift.createConnection('labpc.ipwx.me', 12345, {
  transport : transport,
  protocol : protocol
});

var mp = new thrift.Multiplexer();

// Create a Calculator client with the connection
var client = mp.createClient('Auth', AuthService, connection);

connection.on('error', function(err) {
  if (err) {
    console.log(err);
  }
});

var user = new types.ApiUserCredential({
    'name': 'user',
    'password': 'user123'
});
console.log(user);

client.requestToken(user, function(err, msg) {
  console.log('callback...');
  if (err) {
    console.log('err: ' + err);
  } else {
    console.log('res: ' + msg.token);
  }
  connection.end();
});